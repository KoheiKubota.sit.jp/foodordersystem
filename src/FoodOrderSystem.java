import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderSystem {
    private JPanel root;
    private JButton karaage;
    private JButton syogayaki;
    private JButton yasaiitame;
    private JButton sabanomisoni;
    private JButton sukiyaki;
    private JButton gyudon;
    private JButton CheakOut;
    private JTextPane Itemlist;
    private JLabel TotalYen;
    int yen=0;
    int price(String food){
        if(food == "karaage") {
            return 100;
        }
        else if(food == "syogayaki"){
            return 600;
        }
        else if(food == "yasaiitame"){
            return 500;
        }
        else if(food == "sabanomisoni"){
            return 450;
        }
        else if(food == "sukiyaki"){
            return 1000;
        }
        else if(food == "gyudon"){
            return 700;
        }
        return 0;
    }
    void order(String food) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            yen += price(food);
            TotalYen.setText(yen+" yen");
            String currentText = Itemlist.getText();
            Itemlist.setText(currentText + food +" "+ price(food)+"yen"+"\n");
            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");

        }
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderSystem");
        frame.setContentPane(new FoodOrderSystem().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public FoodOrderSystem() {

        karaage.setIcon(new ImageIcon( this.getClass().getResource("karaage.jpg")));
        syogayaki.setIcon(new ImageIcon( this.getClass().getResource("syogayaki.jpg")));
        yasaiitame.setIcon(new ImageIcon( this.getClass().getResource("yasaiitame.jpg")));
        sabanomisoni.setIcon(new ImageIcon( this.getClass().getResource("sabanomisoni.jpg")));
        sukiyaki.setIcon(new ImageIcon( this.getClass().getResource("sukiyaki.jpg")));
        gyudon.setIcon(new ImageIcon( this.getClass().getResource("gyudon.jpg")));

        karaage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("karaage");
            }
        });
        syogayaki.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("syogayaki");
            }
        });
        yasaiitame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("yasaiitame");
            }
        });
        sabanomisoni.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("sabanomisoni");
            }
        });
        sukiyaki.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("sukiyaki");
            }
        });
        gyudon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("gyudon");
            }
        });
        CheakOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cheakout?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is "+ yen + " yen.");
                    Itemlist.setText(" ");
                    yen = 0;
                    TotalYen.setText(yen+" yen");
                }
            }
        });
    }
}
